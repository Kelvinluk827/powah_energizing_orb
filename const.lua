--- Represent an ingredient in a crafting recipe
---@class Ingredient
---@field name string Item name of the ingredient
---@field count number Number of the ingredient required

--- Represent a crafting recipe in powah thing
---@class Recipe
---@field product string Output product item's name
---@field ingredients Ingredient[] List of ingredient required for the crafting,
--- Where the first ingredient will be used for discriminating element.


-- RECIPES for the energizing orb are put in the list here
-- Each recipe should be written like this:
-- {
--    product = "minecraft:item",
--    ingredients = {
--      {
--        name="mod:item1"
--        count=2
--      },
--      {
--        name="mod:item2"
--        count=4
--      }
--    }
-- }
-- Please note that the first ingredient will be the discriminating element, 
-- i.e. it is used to distinct between recipes when scanning the INPUTINV
---@type Recipe[]
local RECIPES = {
  {
    product = "powah:ender_core",
    ingredients = {
      {
        name="powah:capacitor_basic_tiny",
        count=1
      },
      {
        name="powah:dielectric_casing",
        count=1
      },
      {
        name="minecraft:ender_eye",
        count=1
      }
    }
  },
  {
    product = "powah:uraninite",
    ingredients = {
      {
        name = "emendatusenigmatica:uranium_ingot",
        count = 1
      }
    }
  },
  {
    product = "powah:crystal_niotic",
    ingredients = {
      {
        name = "minecraft:diamond",
        count = 1
      }
    }
  },
  {
    product = "powah:steel_energized",
    ingredients = {
      {
        name = "minecraft:gold_ingot",
        count = 1
      },
      {
        name = "minecraft:iron_ingot",
        count = 1
      }
    }
  },
  {
    product = "powah:dry_ice",
    ingredients = {
      {
        name = "minecraft:blue_ice",
        count = 2
      }
    }
  },
  {
    product = "powah:crystal_nitro",
    ingredients = {
      {
        name = "minecraft:nether_star",
        count = 1
      },
      {
        name = "minecraft:redstone_block",
        count = 2
      },
      {
        name = "powah:blazing_crystal_block",
        count = 1
      }
    }
  },
  {
    product = "powah:blazing_crystal_block",
    ingredients = {
      {
        name = "botania:blaze_block",
        count = 1
      }
    }
  },
  {
    product = "powah:crystal_spirited",
    ingredients = {
      {
        name = "minecraft:emerald",
        count = 1
      }
    }
  }
}

-- This inventory is for placing ingredients by external means
-- Should use exact name, e.g. "minecraft:chest_2"
local INPUTINV = ""

-- This inventory is for the script to place the product to be collected
-- Should use exact name, e.g. "minecraft:chest_2"
local OUTPUTINV = ""

-- Peripheral name of the energizing orb
local ORB = "powah:energizing_orb"

-- This is configuration
---@class Config
---@field interval number Poll interval in second
local CONFIG = {
  interval = 10 -- repeat interval in second
}

---Class to hold the constants required for the code
---@class Const
---@field recipes Recipe[] List of know crafting recipe
---@field inputInv string Peripheral name of the input inventory
---@field outputInv string Peripheral name of the output inventory
---@field orb string Peripheral name of the energizing orb
---@field config Config Configuration
local Const = {}

function Const:new()
  local o = {}
  setmetatable(o, self)
  self.__index = self

  o.recipes = RECIPES
  o.inputInv = INPUTINV
  o.outputInv = OUTPUTINV
  o.orb = ORB
  o.config = CONFIG

  return o
end

---Get list of available products
---@return string[]
function Const:getProducts()
  local products = {}
  for i,r in ipairs(self.recipes) do
    products[i] = r.product
  end
  return products
end

---Get list of discriminating ingredients
---@return string[]
function Const:getDIngredient()
  local di = {}
  for i,r in ipairs(self.recipes) do
    di[i] = r.ingredients[1].name
  end
  return di
end

-- TODO: Add recipe class and put following 3 functions into it

---Get map of ingredient name to count for given recipe
---@param index number Recipe ID (index in the array)
---@return table<string, number>
function Const:getIngredientsMap(index)
  local map = {}
  local ind = self.recipes[index].ingredients
  for _, ingredient in ipairs(ind) do
    map[ingredient.name] = ingredient.count
  end
  return map
end

---Get map of ingredient name to `false` for given recipe
---@param index number Recipe ID (index in the array)
---@return table<string, boolean>
function Const:getBoolIM(index)
  local map = {}
  local ind = self.recipes[index].ingredients
  for _,ingredient in ipairs(ind) do
    map[ingredient.name] = false
  end
  return map
end

---Get array of ingredients for given recipe
---@param index number Recipe ID (index in the array)
---@return string[]
function Const:getIngredients(index)
  local list = {}
  local ind = self.recipes[index].ingredients
  for i,ingredient in ipairs(ind) do
    list[i] = ingredient.name
  end
  return list
end

-- return the object back, like reactjs I guess
return Const
