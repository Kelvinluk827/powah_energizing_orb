local Const = require("const")

---@class App
---@field const Const
---@field inputInv table
---@field outputInv table
---@field orb table
local App = {}

function App:new()
  local o = {}
  setmetatable(o, self)
  self.__index = self
  print("Initialising...")

  -- load const
  o.const = Const:new()
  
  -- chcek if all inventories are ready
  local inputInv = peripheral.wrap(o.const.inputInv)
  if inputInv == nil then
   error("Input inventory is not found.")
  end
  local outputInv = peripheral.wrap(o.const.outputInv)
  if outputInv == nil then
    error("Output inventory is not found.")
  end
  local orb = {peripheral.find(o.const.orb)}
  if #orb ~= 1 then -- There shall be one and only one orb
    error("There shall be one and only one energizing orb.")
  end

  -- Set inventories as data members
  o.inputInv = inputInv
  o.outputInv = outputInv
  o.orb = orb[1] -- There should only be one

  -- Initialized
  print("Initialised.")

  return o
end

---Find and move given item with specified count from input inventory to output inventory.
---
---Note: Caller should ensure item is present in input inventory and output inventory is not full.
---
---@param input table Peripheral wrapper for input inventory
---@param output table Peripheral wrapper for output inventory
---@param itemName string Item name to be moved
---@param count number Number of item to be moved
function App:move(input, output, itemName, count)
  -- loop through the input inventory
  for i = 1, input.size(), 1 do
    local detail = input.getItemDetail(i)
    if detail ~= nil then
      if detail.name == itemName and detail.count >= count then
        input.pushItems(peripheral.getName(output), i, count)
      end
    end
  end
end

---Count number of *occupied* slots in the given inventory
---
---@param invList any[] List of items in the inventory get by `.list()` method
---@param invSize number Size of the inventory get by `.size()` method
---@return number
function App:numOccupied(invList, invSize)
  local count = 0
  for i = 1, invSize, 1 do
    if invList[i] ~= nil then
      count = count + 1
    end
  end
  return count
end

-- This function contains the main logic
function App:logic()
  -- check if the orb is empty
  local orbList = self.orb.list()
  if self:numOccupied(orbList, self.orb.size()) ~= 0 then
    -- clear the orb if there is only product
    if orbList[1] and self:numOccupied(orbList, self.orb.size()) == 1 then -- if it is produced there shall only be one stack
      local products = self.const:getProducts()
      local found = false
      for _, p in ipairs(products) do
        if p==orbList[1].name then
          -- move the item into outputInv and proceed
          -- WARNING: assuming products are always in slot 1
          print(string.format("Retrieving product(s) %s", p))
          self.orb.pushItems(peripheral.getName(self.outputInv), 1)
          found=true
          break
        end
      end
      if not found then
        return
      end
    else
      return
    end
  end

  -- check for discriminating ingredient in inputInv
  local foundDI = nil
  local di = self.const:getDIngredient()
  local inputInvList = self.inputInv.list()
  for it, d in ipairs(di) do
    for i = 1, self.inputInv.size(), 1 do
      if inputInvList[i] ~= nil and inputInvList[i].name == d then
        foundDI = it
        break
      end
    end
  end

  -- if there is DI, then check if all ingredients are here
  -- Todo: account for when items aren't grouped together in 1 stack
  if not foundDI then
    return
  end
  local im = self.const:getIngredientsMap(foundDI)
  local bm = self.const:getBoolIM(foundDI)
  for i = 1, self.inputInv.size(), 1 do
    if inputInvList[i]~=nil and im[inputInvList[i].name]~=nil then
      if inputInvList[i].count >= im[inputInvList[i].name] then
        bm[inputInvList[i].name] = true
      end
    end
  end
  -- check if all items in bm are true
  local il = self.const:getIngredients(foundDI)
  local bmAllTrue = true
  for _, i in ipairs(il) do
    if not bm[i] then
      bmAllTrue = false
    end
  end
  if bmAllTrue then
    -- when all ingredients are here, move them into orb
    local products = self.const:getProducts()
    print(string.format("Crafting %s", products[foundDI]))
    for _, i in ipairs(il) do
      self:move(self.inputInv, self.orb, i, im[i])
    end
  end
end

-- This function keeps on running the logic
function App:gameLoop()
  while true do
    self:logic()
    sleep(self.const.config.interval)
  end
end

-- main function
function main()
  local app = App:new()
  print("Start game loop.")
  app:gameLoop()
end


main()
